import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class AddUserComponent extends Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            walletAmount: '',
            age: '',
            gender: ''
        }
        this.saveUser = this.saveUser.bind(this);
    }

    saveUser = (e) => {
        e.preventDefault();
        let user = {
            name: this.state.name,
            age: this.state.age,
            gender: this.state.gender,
            walletAmount: this.state.walletAmount,
        };
        try {
            user.age = parseInt(user.age);
            user.walletAmount = parseFloat(user.walletAmount);
        } catch (e) {
            throw new Error('Invalid data types passed in the form.');
        }
        ApiService.addPatient(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/users');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <h2 className="text-center">Add Patient</h2>
                <form>
                <div className="form-group">
                    <label>Name:</label>
                    <input type="text" placeholder="Patient name" name="name" className="form-control" value={this.state.name} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Gender:</label>
                    <input placeholder="gender" name="gender" className="form-control" value={this.state.gender} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Age:</label>
                    <input type="number" placeholder="age" name="age" className="form-control" value={this.state.age} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Wallet Amount:</label>
                    <input type="number" placeholder="Amount in wallet (Rs.)" name="walletAmount" className="form-control" value={this.state.walletAmount} onChange={this.onChange}/>
                </div>

                <button className="btn btn-success" onClick={this.saveUser}>Save</button>
            </form>
    </div>
        );
    }
}

export default AddUserComponent;