import React, {Component} from 'react'
import ApiService from "../../service/ApiService";
import RCTable from 'rc-table';

class ListUserComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            users: [],
            message: null
        }
        this.deleteUser = this.deleteUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.reloadUserList = this.reloadUserList.bind(this);
        this.createTableData = this.createTableData.bind(this);
        this.createColumns = this.createColumns.bind(this);
    }

    componentDidMount() {
        this.reloadUserList();
    }

    reloadUserList() {
        ApiService.fetchAll()
            .then((res) => {
                this.setState({users: res.data.patient});
            });
    }

    deleteUser(userId) {
        ApiService.deletePatient(userId)
           .then(res => {
               this.setState({message : 'User deleted successfully.'});
               this.setState({users: this.state.users.filter(user => user.id !== userId)});
               this.reloadUserList();
           })

    }

    editUser(id) {
        window.localStorage.setItem("userId", id);
        this.props.history.push('/edit-user');
    }

    addUser() {
        window.localStorage.removeItem("userId");
        this.props.history.push('/add-user');
    }

    createColumns() {
        var _this = this;
        var cols = [
            {   title: 'Name',
                dataIndex: 'name',
                key: 'name',
                width: 100
            },
            {   title: 'Age',
                dataIndex: 'age',
                key: 'age',
                width: 100
            },
            {   title: 'Gender',
                dataIndex: 'gender',
                key: 'gender',
                width: 100,
            },
            {   title: 'WalletAmount',
                dataIndex: 'walletAmount',
                key: 'walletAmount',
                width: 200,
            },
            {
                title: 'Delete Patient',
                dataIndex: '',
                key: 'delete',
                render: () => <button className="delete-button">Delete</button>,
                width: 300,
                onCell: function (record, index) {
                    return {
                        onClick: async function (e) {
                            console.log('Click cell', ` row ${index}`, record, e.target.className);
                            if (e.target.className.startsWith('delete'))
                                 _this.deleteUser(record.name);
                            // debugger
                            // await _this.reloadUserList();

                        }
                    };
                }
            },
            {
                title: 'Edit Patient',
                dataIndex: '',
                key: 'edit',
                render: () => <button className="edit-button">Edit</button>,
                width: 300,
                onCell: function (record, index) {
                    return {
                        onClick: async function (e) {
                            console.log('Click cell', ` row ${index}`, record, e.target.className);
                            if (e.target.className.startsWith('edit'))
                                _this.editUser(record.name);

                        }
                    };
                }
            },
        ];
        return cols.map((col, id) => {
            return (col.onCell && typeof col.OnCell === 'function') ?
                 col.onCell.bind(this)
                : col;
        });
    }

    createTableData() {
        const res = this.state.users.map((user, index) => {
           return {name: user.name, age: user.age, gender: user.gender, walletAmount: user.walletAmount};
        });
        console.log(JSON.stringify(res));
        return res;
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Patient Details</h2>
                {
                    (Array.isArray(this.state.users) && this.state.users.length > 0) ?
                    <RCTable columns={this.createColumns()} data={this.createTableData()} />
                    : null
                }
                <br></br>
                <button className="btn btn-danger" style={{width:'100px'}} onClick={() => this.addUser()}> Add User</button>
                <br></br>
                {/*<Demo />*/}
            </div>
        );
    }

}

export default ListUserComponent;