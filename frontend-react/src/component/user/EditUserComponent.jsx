import React, { Component } from 'react'
import ApiService from "../../service/ApiService";

class EditUserComponent extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            walletAmount: '',
            age: '',
            gender: ''
        }
        this.saveUser = this.saveUser.bind(this);
        this.loadUser = this.loadUser.bind(this);
        this.patientName = this.patientName.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        ApiService.fetchPatientById(window.localStorage.getItem("userId"))
            .then((res) => {
                debugger
                let user = res.data.patient[0];
                this.setState({
                name: user.name,
                gender: user.gender,
                age: user.age,
                walletAmount: user.walletAmount,
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    patientName = function () {
        return this.state.name;
    }

    saveUser = (e) => {
        e.preventDefault();
         let user = {
            name: this.state.name,
            age: this.state.age,
            gender: this.state.gender,
            walletAmount: this.state.walletAmount,
        };
        try {
            user.age = parseInt(user.age);
            user.walletAmount = parseFloat(user.walletAmount);
        } catch (e) {
            throw new Error('Invalid data types passed in the form.');
        }
        debugger
        var _this = this;
        ApiService.editPatient(user.name, user)
            .then(async function () {
                await _this.props.history.push('/users');
            });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Edit User</h2>
                <form>
                <div>
                    <h2 className="patient-name"> {(typeof this.state.user === 'string' && this.state.user.length > 0) ? this.patientName() : ""} </h2>
                </div>

                <div className="form-group">
                    <label>Gender:</label>
                    <input placeholder="gender" name="gender" className="form-control" value={this.state.gender} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Age:</label>
                    <input type="number" placeholder="age" name="age" className="form-control" value={this.state.age} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Wallet Amount:</label>
                    <input type="number" placeholder="Amount in wallet (Rs.)" name="walletAmount" className="form-control" value={this.state.walletAmount} onChange={this.onChange}/>
                </div>
                    <button className="btn btn-success" onClick={this.saveUser}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditUserComponent;