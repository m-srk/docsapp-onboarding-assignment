import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:3300/api';

class ApiService {

    fetchAll() {
        return axios.get(`${USER_API_BASE_URL}/patients`);
    }

    fetchPatientById(userId) {
        return axios.get(`${USER_API_BASE_URL}/patient/${userId}`);
    }

    deletePatient(userId) {
        return axios.delete(`${USER_API_BASE_URL}/patient/${userId}`);
    }

    addPatient(user) {
        return axios.post(`${USER_API_BASE_URL}/patients`, user);
    }

    editPatient(user, body) {
        return axios.put(`${USER_API_BASE_URL}/patient/${user}`, body);
    }

}

export default new ApiService();