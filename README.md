Run both the frontend and backend modules together.

For backend, run ```npm install``` and then ```node app.js```

For frontend, run ```npm install``` and then ```yarn run start```