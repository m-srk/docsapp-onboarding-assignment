// hardcode dev env for now
const DEFINED_ENV = 'dev';

process.env.NODE_ENV = DEFINED_ENV;

module.exports = {
    secret: (process.env.NODE_ENV === 'production') ?
                process.env.SECRET :
                'secret'
};