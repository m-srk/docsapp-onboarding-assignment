const AWS = require('aws-sdk');

// Configure the region
AWS.config.update({region: 'us-east-2'});

// Create an SQS service object
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});
const queueUrl = "https://sqs.us-east-2.amazonaws.com/427510003639/Srikanth-Onboarding.fifo";

// the new endpoint
let TestPatientData = {
        'name': "chotu",
        'age':"24",
        'gender':"Male",
        'walletAmount':"2450"
    };

// let patientData = {
//         'name':
//         'age':
//         'gender':
//         'walletAmount':
//     };

let sqsPatientData = {
    MessageAttributes: {
      "name": {
        DataType: "String",
        StringValue: TestPatientData.name
      },
      "age": {
        DataType: "Number",
        StringValue: TestPatientData.age
      },
      "gender": {
        DataType: "String",
        StringValue: TestPatientData.gender
      },
      "walletAmount": {
        DataType: "Number",
        StringValue: TestPatientData.walletAmount
      }
    },
    MessageBody: JSON.stringify(TestPatientData),
    MessageDeduplicationId: TestPatientData.name,
    MessageGroupId: "PatientUpdates",
    QueueUrl: queueUrl
};

// Send the data to the SQS queue
let sendSqsMessage = sqs.sendMessage(sqsPatientData).promise();
sendSqsMessage
    .then((data) => {
        console.log(`MESSAGE STATUS | SUCCESS: ${data.name}`);
    }).catch((err) => {
        console.log(`SQS-TEST | ERROR: ${err}`);
        throw err;
    });
