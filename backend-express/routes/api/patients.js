var router = require('express').Router();
const redis = require('redis');
const auth = require('../auth');
const redisClient = redis.createClient();
const REDIS_DELAY = 3600;

(async function injectPatientAPI() {
    const Patient = await require('../../models/Patient');

    // body validation util
    function validateReqBody(body) {
        let name, age, walletAmt, gender = null;
        if (typeof body !== 'object')
            return null;
        if (body.name && typeof body.name === 'string')
            name = body.name;
        if (body.age && typeof body.age === 'number')
            age = body.age;
        if (body.walletAmount && typeof body.walletAmount === 'number')
            walletAmt = body.walletAmount;
        if (body.gender && typeof body.gender === 'string')
            gender = body.gender;
        return {age: age, walletAmt: walletAmt, gender: gender, name: name};
    }

    // redis middle-ware
    function cachePAll(req, res, next) {
        try{
            redisClient.get('all', async function (error, cachedData) {
                if (error) throw error;
                if (!cachedData || (Array.isArray(cachedData) && cachedData.length<1) )
                    next();
                else
                    return res.json({patient: JSON.parse(cachedData)});
            })
        } catch (err) {
            throw err;
        }
        // redisClient
    }

    function cachePOne(req, res, next) {
        try {
            let name = req.params.name;
            var isPost = (req.method.startsWith('POST') === true);
            redisClient.get(name, async function (error, cachedData) {
                if (error) throw error;
                if (!cachedData || (Array.isArray(cachedData) && cachedData.length<1) ) {
                    next();
                }
                else {
                    // kick out duplicate post reqs at very high speed !
                    return (isPost) ?
                        res.sendStatus(401) :
                        res.json({patient: JSON.parse(cachedData)});
                }
            })
        } catch (err) {
            throw err;
        }
    }

    // list get and post
    router.route('/patients')
        .get(cachePAll, async function (req, res, next) {
            try {
                let patients = await Patient.findAll();
                if (!patients)
                    return res.sendStatus(401);
                // add to redis
                redisClient.setex('all', REDIS_DELAY, JSON.stringify(patients));
                return res.json({patient: patients});
            } catch (err) {
                console.log(err);
            }
        })
        .post(async function (req, res, next) {
            try {
                var bodyInfo = null;
                if (!(bodyInfo = validateReqBody(req.body)) )
                    return res.sendStatus(400); // send out 'Bad Request' code

                let patient = await Patient.findAll({where: {name: req.body.name}});
                if (Array.isArray(patient) && patient.length > 0) {
                    // if resource already present, return 401
                    return res.sendStatus(401); // client is unauthorized to create same named patient
                }
                patient = await Patient.create({name: bodyInfo.name, walletAmount: bodyInfo.walletAmt, age: bodyInfo.age, gender: bodyInfo.gender});

                await redisClient.del('all'); // all list invalid now
                redisClient.setex(req.body.name, REDIS_DELAY, JSON.stringify(patient));

                return res.sendStatus(201);
            } catch (e) {
                throw e;
            }
        });

    router.param('name', async function (req, res, next, name) {
        try{
            // kick out any req with illegal path param
            if (!req.params.name || typeof req.params.name !== 'string')
                return res.sendStatus(400);
            next();
        } catch (err) {
            throw err;
            // console.log(err);
        }
    });

    router.route('/patient/:name')
        // individual instance get
        .get(cachePOne, async function (req, res, next) {
            try{
                let patient = await Patient.findAll( {where: {name: req.params.name}});
                if (!Array.isArray(patient) || patient.length < 1)
                    return res.sendStatus(404);
                // add redis key
                redisClient.setex(req.params.name, REDIS_DELAY, JSON.stringify(patient));
                return res.json({patient: patient});
            } catch (err) {
                throw err;
                // console.log(err);
            }
        })
        .put(async function (req, res, next) {
            try{
                const pName = req.params.name;
                let age, walletAmt, gender = null;
                if (req.body.age && typeof req.body.age === 'number')
                    age = req.body.age;
                if (req.body.walletAmount && typeof req.body.walletAmount === 'number')
                    walletAmt = req.body.walletAmount;
                if (req.body.gender && typeof req.body.gender === 'string')
                    gender = req.body.gender;
                let updatedPatient = await Patient.update({age: age, walletAmount: walletAmt, gender: gender, name: pName},
                                            {where: {name: pName}});
                // update redis
                await redisClient.del('all');
                redisClient.setex(pName, REDIS_DELAY, JSON.stringify(updatedPatient));
                return res.sendStatus(200);
            } catch (err) {
                throw err;
            }
        })
        .delete(async function (req, res, next) {
            try{
                const pName = req.params.name;
                var patient = await Patient.findOne({where: {name: pName}});
                if (null === patient)
                    return res.sendStatus(404);
                patient.destroy();
                // remove redis key, cache invalid
                await redisClient.del('all');
                await redisClient.del(pName);
                return res.sendStatus(200);
            } catch (e) {
                throw (e);
            }
        })

    })();

module.exports = router;