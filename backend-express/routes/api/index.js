var router = require('express').Router();

router.use('/', require('./patients'));

// validation error middle ware
router.use(function (err, req, res, next) {
    // eval. node style call back
    if (err.name === 'ValidationError') {
        return res.status(422).json({
            errors: Object.keys(err.errors).reduce(function(errors, key){
                errors[key] = err.errors[key].message;
                return errors;
            }),
        });
    }
    return next(err);
});

module.exports = router;