const express = require('express'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	errorHandler = require('errorhandler');

const isProd = process.env.NODE_ENV === 'production';
const S_HOST = 'localhost';
const S_PORT = 3300;

// Global app object
var app = express();

// // Express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('cors')());

// TODO Setup session
// app.use(session( {
// 	secret: 'secret', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false
// } ));

if (!isProd)
	app.use(errorHandler());

// Register routes in the top level
app.use(require('./routes'));

// TODO Server Error handling
// Dev
// if (!isProd) {
// 	app.use(function(err, req, res, next) {
// 		console.log(err.stack);
// 		res.status(err.status || 500);
// 		res.json({'errors': {
// 				message: err.message,
// 				error: err
// 			}});
// 	});
// }
// // Prod
// app.use(function(err, req, res, next) {
// 	res.status(err.status || 500);
// 	res.json({'errors': {
// 			message: err.message,
// 			error: {}
// 		}});
// });

// Start the server
var onServerOn = function () {
	console.log(`Listening on port: ${server.address().port}`);
	// console.log(`Host address and port: ${server.address()}`);
}
var server = app.listen(process.env.PORT || S_PORT, S_HOST, onServerOn);

