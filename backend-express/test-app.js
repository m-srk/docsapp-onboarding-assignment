var express = require('express');

// Global app object
var app = express();

var router = express.Router();

router.use(function (req, res, next) {
    console.log('%s %s %s', req.method, req.url, req.path);
    next();
})

// this will only be invoked if the path starts with /bar from the mount point
router.get('/', function (req, res, next) {
    // ... maybe some additional /bar logging ...
    next();
})

// // always invoked
// router.use(function (req, res, next) {
//     res.send('Hello World')
// })

app.use('/foo', router)

// Start the server
const S_HOST = 'localhost';
const S_PORT = 4000;

var onServerOn = function () {
    console.log(`Listening on port: ${server.address().port}`);
}

// console.log(`Host address and port: ${server.address()}`);
var server = app.listen(process.env.PORT || S_PORT, S_HOST, onServerOn);
