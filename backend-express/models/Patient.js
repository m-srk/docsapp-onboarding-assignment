const Sequelize = require('sequelize').Sequelize;
const SequelizeDataTypes = require('sequelize').DataTypes;

// TODO move DB_URI to config module
const DB_URI = 'mysql://root:docsapp123@testdb.cvwvaxxojmyx.us-west-2.rds.amazonaws.com/node';
const PATIENT_TABLE_NAME = 'Patients_Srikanth';
const sequelize = new Sequelize(DB_URI);


// attempt to rewrite using aysnc/await TODO
const Patient = (async function () {
    if (!sequelize.isDefined(PATIENT_TABLE_NAME)) {
        var Patient = await sequelize.define(PATIENT_TABLE_NAME,
            {
                name: SequelizeDataTypes.STRING,
                age: SequelizeDataTypes.INTEGER,
                gender: SequelizeDataTypes.STRING, // Should be an ENUM, but not complicating stuff now
                walletAmount: SequelizeDataTypes.INTEGER,
            });
    }

    async function inlineDBTest(printTestRecs=true) {
        try {
            await Patient.bulkCreate([
                { name:'srikanth', age:24, gender:'Male', walletAmount: 5000,},
                { name:'akash', age:26, gender:'Male', walletAmount: 3000,},
                { name:'ashwin', age:25, gender:'Male', walletAmount: 2000,},
            ]);
            let records = await Patient.findAll();
            if (printTestRecs)
                console.log(JSON.stringify(records));
        } catch (err) {
            console.log(err);
        }

    }

// Check DB connection
    try {
        await sequelize.authenticate();
        console.log("DB authentication successful.");
        await sequelize.sync({ force: true })
        console.log("Database & tables created!");

        // Test
        await inlineDBTest(false);
        return Patient;
    } catch (err) {
        console.log("Failed to authenticate DB !");
    }
})();

module.exports = Patient;

